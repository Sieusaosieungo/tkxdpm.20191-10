package entity;

import java.sql.Timestamp;

/**
 * <b>represents a One Way Ticket</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class OneWayTicket extends FareCertificate {
    // information put on ticket
    private Station startStation;
    private Station endStation;
    private double balance;

    public OneWayTicket() {

    }

    public OneWayTicket (String id, String status, Station startStation, Station endStation, double balance) {
        super.id = id;
        super.status = status;
        this.startStation = startStation;
        this.endStation = endStation;
        this.balance = balance;
    }

    public OneWayTicket (String id, String type, String status, Timestamp timeUsed, String code, Station startStation, Station endStation, double balance) {
        super(id, type, status, timeUsed, code);
        this.startStation = startStation;
        this.endStation = endStation;
        this.balance = balance;
    }

    public Station getStartStation() {
        return startStation;
    }

    public Station getEndStation() {
        return endStation;
    }

    public double getBalance() {
        return balance;
    }
}
