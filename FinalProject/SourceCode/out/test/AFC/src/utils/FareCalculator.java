package utils;

/**
 * Payment is interface for payment, It define common payment
 * such as Payment follow zone, Payment follow Distance of 2 stations
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface FareCalculator {
    /**
     * calculate money for the trip
     * @return the price for trip
     */
    double calculateMoney();
}


