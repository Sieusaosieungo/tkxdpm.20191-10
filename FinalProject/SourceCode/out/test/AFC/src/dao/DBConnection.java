package dao;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

/**
 * The DBConnection class is used to connect to the database and execute
 * the query statement (passing it into the query string and returning
 * the query result).
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class DBConnection {

    private Connection conn;
    private static DBConnection dbConnection;
    private PreparedStatement preparedStatement;

    private String URL;
    private String USER;
    private String PASSWORD;

    /**
     * initiate a connection the the database, private constructor to avoid initiating the connection multiple times
     *
     * @throws IOException,SQLException if the process read file or connect to database fails
     */
    private DBConnection() {
        Properties properties = new Properties();

        try {
            properties.load(new FileReader(new File("resources/config/dbConfig.properties")));
            URL = properties.getProperty("URL");
            USER = properties.getProperty("USER");
            PASSWORD = properties.getProperty("PASSWORD");

            // driver register
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            this.conn = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method allow to create a connection to the database
     *
     * @return MysqlConnect Database connection object
     */
    public static synchronized DBConnection getConnection() {
        if (dbConnection == null) {
            dbConnection = new DBConnection();
        }
        return dbConnection;
    }

    /**
     * Method to query data in the table, include add, update, delete and select
     *
     * @param query String the query
     * @return prepareStatement allows to execute SQL queries
     * @throws SQLException if the query to the database fails
     */
    public PreparedStatement query(String query) throws SQLException {
        preparedStatement = dbConnection.conn.prepareStatement(query);
        return preparedStatement;
    }
}