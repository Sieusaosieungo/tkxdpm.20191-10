package dao;

import entity.Ticket24Hours;

import java.sql.Timestamp;

/**
 * TFDao is the interface used to communicate with 24hour ticket data
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface TFDao {
    /**
     * get ticket object when having the ticket id
     *
     * @param ticketCode This is the 24hours ticket code.
     * @return A 24hours ticket object includes all the information of
     * the 24hours ticket object
     */
    Ticket24Hours getInstanceByCode(String ticketCode);

    /**
     * Update ticket status when checkin
     *
     * @param ticketId This is the ticket id of ticket.
     * @return true if updating is succesful
     */
    boolean updateStatusAfterCheckin(String ticketId);

    /**
     * Update ticket status when checkout
     *
     * @param ticketId This is the ticket id of ticket.
     * @return true if updating is succesful
     */
    boolean updateStatusAfterCheckout(String ticketId);

    /**
     * Update expiration time
     * @param ticketId The id of the ticket
     * @return Expired time
     */
    Timestamp updateTimeExpire(String ticketId);
}
