package control;

import dao.*;
import entity.*;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;
import utils.FareCalculator;
import utils.FareCalculatorDistance;
import utils.StringCase;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The AfcControl class is used to control the overall processing of the system,
 * receive user information, call the corresponding controllers and return
 * corresponding data to the UserInterface.
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class AfcControl {

    // if user checkin statusCheck = 1, if user checkout statusCheck = 2
    private int statusCheck;

    // id of the station that the user is checking in or checking out
    private int stationId;

    // The barcode that the user enters
    private String barcode;

    // List of barcode in file
    private List<String> listBarcode = new ArrayList<>();

    // ticket object that the user uses to checkin or checkout
    private FareCertificate fareCertificate;

    // The object represents the fare certificate control class
    private FareCertificateControl fareCertificateControl;

    /**
     * constructor default
     */
    public AfcControl() {

    }

    /**
     * Constructor for AfcControl
     *
     * @param statusCheck if user checkin statusCheck = 1, if user checkout statusCheck = 2
     * @param stationId id of the station that the user is checking in or checking out
     * @param barcode The barcode that the user enters
     */
    public AfcControl(int statusCheck, int stationId, String barcode) {
        this.statusCheck = statusCheck;
        this.stationId = stationId;
        this.barcode = barcode;
    }

    /**
     *
     * @return the ticket object the user is using
     */
    public FareCertificate getFareCertificate() {
        return fareCertificate;
    }

    /**
     *
     * @return List of barcode users have
     */
    public List<String> getListBarcode() {
        return listBarcode;
    }

    /**
     *
     * @return List of stations of the system
     */
    public List<Station> getListStation() {
        StationDao stationDao = new StationDaoImpl();
        List<Station> stations = stationDao.getAllStation();
        return stations;
    }

    /**
     *
     * @return List the information of ticket from file
     */
    public List<FareCertificate> getInfoFromFile() {
        BufferedReader fileReader = null;
        List<FareCertificate> listItemInFile = new ArrayList<>();
        try {
            String strCurrentLine;

            fileReader = new BufferedReader(new FileReader("resources/user_item"));

            while ((strCurrentLine = fileReader.readLine()) != null) {
                this.listBarcode.add(strCurrentLine);
                String code;
                StringCase stringCase = StringCase.getInstance();
                if (stringCase.isLowerCase(strCurrentLine)) {
                    TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
                    code = ticketRecognizer.process(strCurrentLine);

                    TypeDao typeDao = new TypeDaoImpl();
                    String type = typeDao.getType(code);

                    if (type.equals("oneway")) {
                        OWDao owDao = new OWDaoImpl();
                        OneWayTicket ticket = owDao.getInstanceByCode(code);
                        listItemInFile.add(ticket);
                    }
                    if (type.equals("24hour")) {
                        TFDao tfDao = new TFDaoImpl();
                        Ticket24Hours ticket = tfDao.getInstanceByCode(code);
                        listItemInFile.add(ticket);
                    }

                } else if (stringCase.isUpperCase(strCurrentLine)) {
                    CardScanner cardScanner = CardScanner.getInstance();
                    code = cardScanner.process(strCurrentLine);

                    TypeDao typeDao = new TypeDaoImpl();
                    String type = typeDao.getType(code);

                    if (type.equals("prepaid")) {
                        PPDao ppDao = new PPDaoImpl();
                        PrepaidCard card = ppDao.getInstanceByCode(code);
                        listItemInFile.add(card);
                    }
                }
            }
        } catch (IOException | InvalidIDException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null)
                    fileReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return listItemInFile;
    }

    /**
     * perform checkin or user checkout
     *
     * @return error message if an error occurs, if no error returns null
     * @throws InvalidIDException If the barcode code is converted into a card code or ticket code error occurs
     */
    public String check() throws InvalidIDException {
        StationDao stationDao = new StationDaoImpl();
        Station station = stationDao.getStationById(this.stationId);
        String code;
        String error = null;
        StringCase stringCase = StringCase.getInstance();
        double distance = 0;

        if (stringCase.isLowerCase(this.barcode)) {
            TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
            code = ticketRecognizer.process(this.barcode);

            TypeDao typeDao = new TypeDaoImpl();
            String type = typeDao.getType(code);

            if (type.equals("oneway")) {
                OWDao owDao = new OWDaoImpl();
                OneWayTicket oneWayTicket = owDao.getInstanceByCode(code);
                fareCertificateControl = new OneWayControl(oneWayTicket, station);
                this.fareCertificate = oneWayTicket;

                if (this.statusCheck == 1) {
                    error = fareCertificateControl.checkin();
                }
                if (this.statusCheck == 2) {
                    DistanceDao distanceDao = new DistanceImpl();
                    distance = distanceDao.getDistance(fareCertificate,station);
                    FareCalculator fareCalculator = new FareCalculatorDistance(distance);
                    fareCertificateControl.setFareCalculator(fareCalculator);

                    error = fareCertificateControl.checkout();
                }

                return error;
            }

            if (type.equals("24hour")) {
                TFDao tfDao = new TFDaoImpl();
                Ticket24Hours ticket24Hours = tfDao.getInstanceByCode(code);
                fareCertificateControl = new Ticket24Control(ticket24Hours, station);
                this.fareCertificate = ticket24Hours;
                if (this.statusCheck == 1) {
                    error = fareCertificateControl.checkin();
                }
                if (this.statusCheck == 2) {
                    error = fareCertificateControl.checkout();
                }

                return error;
            }
        } else if (stringCase.isUpperCase(this.barcode)) {
            CardScanner cardScanner = CardScanner.getInstance();
            code = cardScanner.process(this.barcode);
            TypeDao typeDao = new TypeDaoImpl();
            String type = typeDao.getType(code);

            if (type.equals("prepaid")) {
                PPDao ppDao = new PPDaoImpl();
                PrepaidCard prepaidCard = ppDao.getInstanceByCode(code);
                fareCertificateControl = new PrepaidControl(prepaidCard, station);
                this.fareCertificate = prepaidCard;

                if (this.statusCheck == 1) {
                    error = fareCertificateControl.checkin();
                }
                if (this.statusCheck == 2) {
                    DistanceDao distanceDao = new DistanceImpl();
                    distance = distanceDao.getDistance(fareCertificate,station);
                    FareCalculator fareCalculator = new FareCalculatorDistance(distance);
                    fareCertificateControl.setFareCalculator(fareCalculator);

                    error = fareCertificateControl.checkout();
                }

                return error;
            }
        }

        return "Barcode invalid";
    }
}
