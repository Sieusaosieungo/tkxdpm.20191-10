package entity;

/**
 * <b>represents a Station</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class Station {
    private int id;
    private String name;

    public Station() {

    }

    public Station(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

}
