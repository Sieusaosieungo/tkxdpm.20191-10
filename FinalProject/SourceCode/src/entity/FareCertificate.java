package entity;

import java.sql.Timestamp;

/**
 * <b>represents a Ticket common includes Prepaid Card, OneWayTicket, Ticket 24 Hours, ...</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class FareCertificate {
    protected String id;
    protected String type;
    protected String status;
    protected Timestamp timeUsed;
    protected String code;

    public FareCertificate() {}

    public FareCertificate(String id, String type, String status, Timestamp timeUsed, String code) {
        this.id = id;
        this.type = type;
        this.status = status;
        this.timeUsed = timeUsed;
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
