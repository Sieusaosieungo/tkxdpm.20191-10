# Phân công nhiệm vụ

## Tuần 1

### 1. Phân công công việc

- Vũ Duy Mạnh: Luồng sự kiện Mua vé, Thanh toán
- Ngô Đức Minh: Luồng sự kiện Mua thẻ
- UN Lyan: Luồng sự kiện In vé, Xuất thẻ
- Nguyễn Thành Luân: Luồng sự kiện Nạp thẻ

### 2. Phân công review

- Mạnh review cho Minh
- Minh review cho Luân
- Luân review cho An
- An review cho Mạnh

## Tuần 2

### 1. Phân công công việc

- Vũ Duy Mạnh: Đặc tả checkout vé, từ điển thuật ngữ, Functionality
- Ngô Đức Minh: Đặc tả checkout thẻ, Usability (Tính dễ dùng)
- UN Lyan: Đặc tả checkin vé, Reliable(tính tin cậy), Performance (Hiệu năng)
- Nguyễn Thành Luân: Đặc tả checkin thẻ, Supportability (Tính bảo trì, hỗ trợ sau khi bàn giao)

### 2. Phân công review

- Minh review cho An
- An review cho Mạnh
- Mạnh review cho Luân
- Luân review cho Minh

## Tuần 3

### 1. Phân công công việc

- Vũ Duy Mạnh: Sửa usecase, sửa acitivty diagram, bổ sung thêm đặc tả bằng bảng, text. Vẽ biểu đồ tương tác và biểu đồ lớp cho check-out vé
- Ngô Đức Minh: Vẽ biểu đồ tương tác và biểu đồ lớp cho check-out thẻ
- UN Lyan: Vẽ biểu đồ tương tác và biểu đồ lớp cho quét vé, đọc thẻ và check-in thẻ
- Nguyễn Thành Luân: Vẽ biểu đồ tương tác và biểu đồ lớp cho check-in vé

### 2. Phân công review

- Minh review cho An
- An review cho Mạnh
- Mạnh review cho Luân
- Luân review cho Minh

## Tuần 4

### 1. Phân công công việc

- Vũ Duy Mạnh: Vẽ sơ đồ thực thể liên kết, thiết kế database
- Ngô Đức Minh: Thiết kế giao diện
- UN Lyan: Vẽ biểu đồ lớp chi tiết các chức năng liên quan đến thẻ
- Nguyễn Thành Luân: Vẽ biểu đồ lớp chi tiết các chức năng liên quan đến vé

### 2. Phân công review

- Minh review cho An
- An review cho Mạnh
- Mạnh review cho Luân
- Luân review cho Minh

## Tuần 5

### 1. Phân công công việc

- Tất cả các thành viên sửa lại toàn bộ các phần đã làm theo biểu đồ UC đã chỉnh sửa
- Vũ Duy Mạnh: Usecase checkin vé 1 chiều, checkin vé 24 giờ + thiết kế UI
- Ngô Đức Minh: Usecase checkout thẻ
- UN Lyan: Usecase checkin thẻ
- Nguyễn Thành Luân: Usecase checkout vé 1 chiều, checkout vé 24 giờ
- Cả nhóm thiết kế database

### 2. Phân công review

- Minh review cho An
- An review cho Mạnh
- Mạnh review cho Luân
- Luân review cho Minh

## Tuần 6

### 1. Phân công công việc

- Các thành viên code theo các chức năng đã nhận của tuần 5

# Đánh giá sự hoàn thành

## Vũ Duy Mạnh : 90%

## Ngô Đức Minh : 0%

## Nguyễn Thành Luân : 0%

## Un Lyan : 0%

# Đóng góp của các thành viên vào project

## Vũ Duy Mạnh: 98%

## Ngô Đức Minh : 2%

## Nguyễn Thành Luân : 0%

## Un Lyan : 0%
