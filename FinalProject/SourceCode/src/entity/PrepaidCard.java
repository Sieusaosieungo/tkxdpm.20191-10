package entity;

import java.sql.Timestamp;
import java.text.DecimalFormat;

/**
 * <b>represents a Prepaid Card</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class PrepaidCard extends FareCertificate {

    // The balance of the prepaid card
    private double balance;

    public PrepaidCard() {

    }

    public PrepaidCard(String id, String status, double balance) {
        super.id = id;
        super.status = status;
        this.balance = balance;
    }

    public PrepaidCard(String id, String type, String status, Timestamp timeUsed, String code, double balance) {
        super(id, type, status, timeUsed, code);
        this.balance = balance;
    }

    /**
     * @return balance for prapaid card
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Update balance when pay by prepaid card
     *
     * @param amount Total price for trip
     */
    public void decreaseBalance(double amount) {
        if (amount < 0) {
            throw new Error("Amount invalid! Money for trip can not negative");
        }
        double balanceFormat = this.balance - amount;
        DecimalFormat df = new DecimalFormat("###.##");

        this.balance = Double.parseDouble(df.format(balanceFormat));
    }
}
