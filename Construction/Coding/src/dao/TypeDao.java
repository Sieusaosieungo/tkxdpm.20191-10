package dao;

/**
 * TypeDao is the interface used to communicate with the type of ticket, card data
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface TypeDao {
    /**
     *
     * @param code The code of the ticket of card
     * @return Ticket or card
     */
    String getType(String code);
}
