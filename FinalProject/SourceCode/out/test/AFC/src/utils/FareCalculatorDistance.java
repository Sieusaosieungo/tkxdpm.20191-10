package utils;

import dao.DBConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

/**
 * implement methods of the Payment interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class FareCalculatorDistance implements FareCalculator {
    private double distance;

    public FareCalculatorDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public double calculateMoney() {
        double baseFare = 0;
        double threshold = 0;
        DBConnection dbConnection = DBConnection.getConnection();

        String query = "SELECT * FROM Threshold";

        PreparedStatement prst;
        try {
            prst = dbConnection.query(query);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                threshold = rs.getDouble(1);
                baseFare = rs.getDouble(2);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        if (distance > threshold) {
            if (distance - (int) distance != 0) {
                baseFare += ((int) ((distance - threshold) / 2) + 1) * 0.4;
            } else {
                baseFare += (int) ((distance - threshold) / 2) * 0.4;
            }
        }

        DecimalFormat df = new DecimalFormat("###.##");

        return Double.parseDouble(df.format(baseFare));
    }
}
