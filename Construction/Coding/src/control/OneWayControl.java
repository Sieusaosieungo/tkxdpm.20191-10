package control;

import dao.*;
import entity.OneWayTicket;
import entity.Station;

import java.util.List;

/**
 * OneWayControl class is used to checkin or checkout with the user's one-way ticket
 * object and return an error message if any
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class OneWayControl extends FareCertificateControl {

    // The object represent for OneWayTicket class
    private OneWayTicket oneWayTicket;

    // The total amount for the trip
    private double amountFare;

    /**
     * This constructor initializes a control object for one-way tickets
     *
     * @param oneWayTicket The object of OneWayTicket class
     * @param station      The object of Station class
     */
    public OneWayControl(OneWayTicket oneWayTicket, Station station) {
        this.oneWayTicket = oneWayTicket;
        super.station = station;
    }

    @Override
    public String checkin() {
        String error = this.checkValidWhenCheckin();

        if (error != null && error.equals("error-status")) {
            return "The ticket cannot enter";
        } else if (error != null && error.equals("error-station-enter")) {
            return "You can't enter this station";
        } else {
            try {
                TripDao tripDao = new TripDaoImpl();
                if (!tripDao.createTripWhenCheckin(super.station.getId(), oneWayTicket.getId())) {
                    return "create trip failed";
                }

                OWDao owDao = new OWDaoImpl();
                if (!owDao.updateStatusAfterCheckin(oneWayTicket.getId())) {
                    return "update status failed";
                }
                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }
    }

    @Override
    public String checkout() {
        String error = this.checkValidWhenCheckout();
        if (error != null && error.equals("error-status")) {
            return "Ticket haven't entrance, so it cannot go out";
        } else if (error != null && error.equals("error-balance")) {
            return "Not enough balance: Expected " + this.amountFare + " euros";
        } else {
            try {
                TripDao tripDao = new TripDaoImpl();
                if (!tripDao.updateTripWhenCheckout(super.station.getId(), oneWayTicket.getId())) {
                    return "update trip failed";
                }

                OWDao owDao = new OWDaoImpl();
                if (!owDao.updateStatusAfterCheckout(oneWayTicket.getId())) {
                    return "update status failed";
                }
                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }
    }

    /**
     * Check if the ticket is valid before checkin
     *
     * @return error message if an error occurs, otherwise returns null
     */
    protected String checkValidWhenCheckin() {
        if (!this.isCheckinStatus()) {
            return "error-status";
        } else if (!this.checkStationCanEnter(super.station, oneWayTicket.getStartStation(), oneWayTicket.getEndStation())) {
            return "error-station-enter";
        }
        return null;
    }

    /**
     * Check if the ticket is valid before checkout
     *
     * @return error message if an error occurs, otherwise returns null
     */
    protected String checkValidWhenCheckout() {
        if (!this.isCheckoutStatus()) {
            return "error-status";
        }
        if (!this.checkAmountFare()) {
            return "error-balance";
        }
        return null;
    }

    /**
     * Check if the current ticket status is a checkin status
     *
     * @return true if the current status of the ticket is valid, otherwise returns false
     */
    protected boolean isCheckinStatus() {
        if (oneWayTicket.getStatus().equals("new")) {
            return true;
        }
        return false;
    }

    /**
     * Check if the current ticket status is a checkout status
     *
     * @return true if the current status of the ticket is valid, otherwise returns false
     */
    protected boolean isCheckoutStatus() {
        System.out.println(oneWayTicket.getStatus());

        if (oneWayTicket.getStatus().equals("using")) {
            return true;
        }
        return false;
    }

    /**
     * Check if the current station is accessible, provided that it is within 2 terminals of the ticket
     *
     * @param station      current station
     * @param startStation The departure station is indicated on the ticket
     * @param endStation   End terminal is indicated on the ticket
     * @return true if it is possible to access the current station
     */
    private boolean checkStationCanEnter(Station station, Station startStation, Station endStation) {
        StationDao stationDao = new StationDaoImpl();
        List<Station> stations = stationDao.getStationBetween(startStation, endStation);

        for (int i = 0; i < stations.size(); i++) {
            if (stations.get(i).getId() == station.getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the fare is enough for the trip
     *
     * @return true if enough money
     */
    private boolean checkAmountFare() {
        double amountFare = fareCalculator.calculateMoney();
        this.amountFare = amountFare;

        return oneWayTicket.getBalance() >= amountFare;
    }
}
