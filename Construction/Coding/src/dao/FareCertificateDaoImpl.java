package dao;

import utils.DateHandler;

import java.sql.PreparedStatement;
import java.sql.Timestamp;

/**
 * implement some common methods of ticket types
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class FareCertificateDaoImpl {
    public boolean updateStatusAfterCheckin(String ticketId) {
        DateHandler dateHandler = DateHandler.getInstance();
        Timestamp currentTime = dateHandler.getCurrentTime();
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "UPDATE `AFC`.`Ticket` SET `status` = ?, `timeUsed` = ? WHERE (`id` = ?);\n";

            PreparedStatement prst = dbConnection.query(query);

            prst.setString(1, "using");
            prst.setTimestamp(2, currentTime);
            prst.setString(3, ticketId);

            prst.executeUpdate();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateStatusAfterCheckout(String ticketId) {
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "UPDATE `AFC`.`Ticket` SET `status` = ? WHERE (`id` = ?);\n";

            PreparedStatement prst = dbConnection.query(query);

            prst.setString(1, "came-out");
            prst.setString(2, ticketId);

            prst.executeUpdate();
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return false;
    }
}
