package control;

import dao.TFDao;
import dao.TFDaoImpl;
import dao.TripDao;
import dao.TripDaoImpl;
import entity.Ticket24Hours;
import entity.Station;
import utils.DateHandler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Ticket24Control class is used to checkin or checkout with the user's 24Hours Ticket
 * object and return an error message if any
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class Ticket24Control extends FareCertificateControl {

    // The object represent for Ticket24Hours class
    private Ticket24Hours ticket24Hours;

    /**
     * This constructor initializes a control object for 24hours tickets
     *
     * @param ticket24Hours The object of Ticket24Hours class
     * @param station       The object of Station class
     */
    public Ticket24Control(Ticket24Hours ticket24Hours, Station station) {
        this.ticket24Hours = ticket24Hours;
        super.station = station;
    }

    @Override
    public String checkin() {
        String error = this.checkValidWhenCheckin();

        if (error != null && error.equals("error-status")) {
            return "The ticket cannot enter";
        } else if (error != null && error.equals("error-time-expire")) {
            DateHandler dateHandler = DateHandler.getInstance();
            Timestamp currentTime = dateHandler.getCurrentTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
            return "Expired: Try to enter at " + dateFormat.format(currentTime);
        } else {
            try {
                TripDao tripDao = new TripDaoImpl();
                if (!tripDao.createTripWhenCheckin(super.station.getId(), ticket24Hours.getId())) {
                    return "create trip failed";
                }

                TFDao tfDao = new TFDaoImpl();

                if (ticket24Hours.getTimeExpire() == null) {
                    Timestamp timeExpire = tfDao.updateTimeExpire(ticket24Hours.getId());
                    ticket24Hours.setTimeExpire(timeExpire);
                }

                if (!tfDao.updateStatusAfterCheckin(ticket24Hours.getId())) {
                    return "update status failed";
                }

                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }
    }

    @Override
    public String checkout() {
        String error = this.checkValidWhenCheckout();

        if (error != null && error.equals("error-status")) {
            return "Ticket haven't entrance, so it cannot go out";
        } else {
            try {
                TripDao tripDao = new TripDaoImpl();
                if (!tripDao.updateTripWhenCheckout(super.station.getId(), ticket24Hours.getId())) {
                    return "update trip failed";
                }
                TFDao tfDao = new TFDaoImpl();
                if (!tfDao.updateStatusAfterCheckout(ticket24Hours.getId())) {
                    return "update status failed";
                }

                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }
    }

    /**
     * Check if the ticket is valid before checkin
     *
     * @return error message if an error occurs, otherwise returns null
     */
    protected String checkValidWhenCheckin() {
        if (!this.isCheckinStatus()) {
            return "error-status";
        } else if (!this.checkTimeExpire()) {
            return "error-time-expire";
        }
        return null;
    }

    /**
     * Check if the ticket is valid before checkout
     *
     * @return error message if an error occurs, otherwise returns null
     */
    protected String checkValidWhenCheckout() {
        if (!this.isCheckoutStatus()) {
            return "error-status";
        }
        return null;
    }

    /**
     * Check if the current ticket status is a checkin status
     *
     * @return true if the current status of the ticket is valid, otherwise returns false
     */
    protected boolean isCheckinStatus() {
        if (!ticket24Hours.getStatus().equals("using")) {
            return true;
        }
        return false;
    }

    /**
     * Check if the current ticket status is a checkout status
     *
     * @return true if the current status of the ticket is valid, otherwise returns false
     */
    protected boolean isCheckoutStatus() {
        if (ticket24Hours.getStatus().equals("using")) {
            return true;
        }
        return false;
    }

    /**
     * Check if the current ticket is still valid
     *
     * @return true if the ticket is expired, otherwise it returns false
     */
    private boolean checkTimeExpire() {
        DateHandler dateHandler = DateHandler.getInstance();
        Timestamp currentTime = dateHandler.getCurrentTime();
        Timestamp timeExpire = ticket24Hours.getTimeExpire();

        if (timeExpire == null) {
            return true;
        }

        if (currentTime.compareTo(timeExpire) < 0) {
            return true;
        }

        return false;
    }
}
