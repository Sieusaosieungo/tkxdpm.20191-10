package utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * <b>represent object handle time like convert , format ...</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class DateHandler {
    private static DateHandler myInstance;

    private DateHandler() {
    }

    /**
     *
     * @return an instance for DateHandler
     */
    public static DateHandler getInstance() {
        if(myInstance == null) {
            myInstance = new DateHandler();
        }
        return myInstance;
    }

    /**
     * get time currently follow yyyy-MM-dd HH:mm:ss
     *
     * @return time currently follow yyyy-MM-dd HH:mm:ss
     */
    public Timestamp getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date currentTime = new java.util.Date();
        dateFormat.format(currentTime);

        Timestamp timestamp = new Timestamp(currentTime.getTime());
        return timestamp;
    }

    /**
     * adding time and format
     *
     * @param rootTime      time root
     * @param numberOfHours time adding
     * @return new time after adding follow format
     */
    public Timestamp plusTime(Timestamp rootTime, int numberOfHours) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(rootTime);
        cal.add(Calendar.HOUR, numberOfHours);

        java.util.Date newTime = cal.getTime();
        dateFormat.format(newTime);

        Timestamp timestamp = new Timestamp(newTime.getTime());

        return timestamp;
    }
}