package dao;

import entity.Station;

/**
 * TripDao is the interface used to communicate with trip data
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface TripDao {

    /**
     * create the new trip include time in and station started when checkin
     *
     * @param startStation the id of the station when checkin
     * @param ticketId This is the ticket id of ticket. Ex: OW201910220000
     * @return true if create successfully
     */
    boolean createTripWhenCheckin(int startStation, String ticketId);

    /**
     * Get station information when checkin
     * @param ticketId This is the ticket id of ticket. Ex: OW201910220000
     * @return station information when checkin
     */
    Station getStartStation(String ticketId);

    /**
     *
     * @param endStation the id of the station when checkout
     * @param ticketId This is the ticket id of ticket. Ex: OW201910220000
     * @return true if update succesfully
     */
    boolean updateTripWhenCheckout(int endStation, String ticketId);
}
