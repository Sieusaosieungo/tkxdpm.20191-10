package utils;

/**
 * <b>represent object handle String</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class StringCase {
    private static StringCase myInstance;

    private StringCase() {
    }

    /**
     *
     * @return an instance for StringCase
     */
    public static StringCase getInstance() {
        if(myInstance == null) {
            myInstance = new StringCase();
        }
        return myInstance;
    }
    /**
     * check uppercase
     *
     * @param s string need handle
     * @return true if String is uppercase
     */
    public boolean isUpperCase(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isUpperCase(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * check lowercase
     *
     * @param s string need handle
     * @return true if String is lowercase
     */
    public boolean isLowerCase(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLowerCase(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
