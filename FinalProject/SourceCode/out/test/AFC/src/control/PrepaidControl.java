package control;

import dao.*;
import entity.PrepaidCard;
import entity.Station;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * PrepaidControl class is used to checkin or checkout with the user's Prepaid card
 * object and return an error message if any
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class PrepaidControl extends FareCertificateControl {

    // The object represent for OneWayTicket class
    private PrepaidCard prepaidCard;

    // The total amount for the trip
    private double amountFare;

    /**
     * This constructor initializes a control object for prepaid card
     *
     * @param prepaidCard The object of PrepaidCard class
     * @param station     The object of Station class
     */
    public PrepaidControl(PrepaidCard prepaidCard, Station station) {
        this.prepaidCard = prepaidCard;
        super.station = station;
    }

    @Override
    public String checkin() {
        String error = this.checkValidWhenCheckin();

        if (error != null && error.equals("error-status")) {
            return "The card cannot enter";
        } else if (error != null && error.equals("error-base-fare")) {
            return "Balance on the card is less than the base fare";
        } else {
            try {
                TripDao tripDao = new TripDaoImpl();
                if (!tripDao.createTripWhenCheckin(super.station.getId(), prepaidCard.getId())) {
                    return "create trip failed";
                }

                PPDao ppDao = new PPDaoImpl();
                if (!ppDao.updateStatusAfterCheckin(prepaidCard.getId())) {
                    return "update status failed";
                }
                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }
    }

    @Override
    String checkout() {
        String error = this.checkValidWhenCheckout();

        if (error != null && error.equals("error-status")) {
            return "The card cannot came out";
        } else if (error != null && error.equals("error-amount-fare")) {
            return "Not enough balance: Expected " + this.amountFare + " euros";
        } else {
            try {
                TripDao tripDao = new TripDaoImpl();
                if (!tripDao.updateTripWhenCheckout(super.station.getId(), prepaidCard.getId())) {
                    return "update trip failed";
                }

                PPDao ppDao = new PPDaoImpl();
                if (!ppDao.updateStatusAfterCheckout(prepaidCard.getId())) {
                    return "update status failed";
                }

                prepaidCard.decreaseBalance(amountFare);
                if (!ppDao.updateBalance(prepaidCard.getId(), prepaidCard.getBalance())) {
                    return "update balance failed";
                }

                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }
    }

    /**
     * Check if the card is valid before checkin
     *
     * @return error message if an error occurs, otherwise returns null
     */
    protected String checkValidWhenCheckin() {
        if (!this.isCheckinStatus()) {
            return "error-status";
        }

        if (!this.checkBaseFare()) {
            return "error-base-fare";
        }

        return null;
    }

    /**
     * Check if the card is valid before checkout
     *
     * @return error message if an error occurs, otherwise returns null
     */
    protected String checkValidWhenCheckout() {
        if (!this.isCheckoutStatus()) {
            return "error-status";
        }

        if (!this.checkAmountFare()) {
            return "error-amount-fare";
        }

        return null;
    }

    /**
     * Check if the current card status is a checkin status
     *
     * @return true if the current status of the card is valid, otherwise returns false
     */
    protected boolean isCheckinStatus() {
        if (prepaidCard.getStatus().equals("new")) {
            return true;
        }
        return false;
    }

    /**
     * Check if the current card status is a checkout status
     *
     * @return true if the current status of the card is valid, otherwise returns false
     */
    protected boolean isCheckoutStatus() {
        if (prepaidCard.getStatus().equals("using")) {
            return true;
        }
        return false;
    }

    /**
     * Check if the balance in the card is greater than the base price
     *
     * @return true if enough money
     */
    private boolean checkBaseFare() {
        double baseFare = 0;
        DBConnection dbConnection = DBConnection.getConnection();

        String query = "SELECT * FROM Threshold";

        PreparedStatement prst;
        try {
            prst = dbConnection.query(query);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                baseFare = rs.getDouble(2);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return prepaidCard.getBalance() >= baseFare;
    }

    /**
     * Check if the fare is enough for the trip
     *
     * @return true if enough money
     */
    private boolean checkAmountFare() {
        double amountFare = fareCalculator.calculateMoney();
        this.amountFare = amountFare;

        return prepaidCard.getBalance() >= amountFare;
    }
}
