package dao;

import entity.Station;
import utils.DateHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * implement methods of the TripDao interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class TripDaoImpl implements TripDao {
    @Override
    public boolean createTripWhenCheckin(int startStation, String ticketId) {
        DateHandler dateHandler = DateHandler.getInstance();
        Timestamp currentTime = dateHandler.getCurrentTime();
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "INSERT INTO `AFC`.`Trip` (`timeIn`, `stationStarted`, `ticket_id`) VALUES (?, ?, ?);\n";

            PreparedStatement prst = dbConnection.query(query);

            prst.setTimestamp(1, currentTime);
            prst.setInt(2, startStation);
            prst.setString(3, ticketId);
            prst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Station getStartStation(String ticketId) {
        Station station = null;

        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "SELECT st.id, st.name FROM AFC.Trip trip, AFC.Station st\n" +
                    "where trip.stationEnded is null and trip.ticket_id = '" + ticketId + "' and trip.stationStarted = st.id;";
            PreparedStatement prst = dbConnection.query(query);
            ResultSet rs = prst.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                station = new Station(id, name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return station;
    }


    @Override
    public boolean updateTripWhenCheckout(int stationEnded, String ticketId) {
        DateHandler dateHandler = DateHandler.getInstance();
        Timestamp currentTime = dateHandler.getCurrentTime();
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query =
                    "UPDATE Trip, Ticket \n" +
                            "SET Trip.timeOut = ?, trip.stationEnded = ?\n" +
                            "WHERE Trip.ticket_id = Ticket.id and Trip.timeOut is null and Ticket.id = ? ";

            PreparedStatement prst = dbConnection.query(query);

            prst.setTimestamp(1, currentTime);
            prst.setInt(2, stationEnded);
            prst.setString(3, ticketId);
            prst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
