## Tuần 1
### 1. Phân công công việc 
* Vũ Duy Mạnh: Luồng sự kiện Mua vé, Thanh toán
* Ngô Đức Minh: Luồng sự kiện Mua thẻ
* UN Lyan: Luồng sự kiện In vé, Xuất thẻ
* Nguyễn Thành Luân: Luồng sự kiện Nạp thẻ

### 2. Phân công review
* Mạnh review cho Minh
* Minh review cho Luân
* Luân review cho An
* An review cho Mạnh