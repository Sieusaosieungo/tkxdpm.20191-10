package dao;

import entity.Station;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * implement methods of the StationDao interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class StationDaoImpl implements StationDao {

    @Override
    public List<Station> getAllStation() {
        List<Station> stations = new ArrayList<>();
        Station station;
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            PreparedStatement prst = dbConnection.query("SELECT id, name FROM station");

            ResultSet rs = prst.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                station = new Station(id, name);
                stations.add(station);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return stations;
    }

    @Override
    public Station getStationById(int stationId) {
        Station station = null;
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            PreparedStatement prst = dbConnection.query("SELECT * FROM station WHERE id = ?");

            prst.setInt(1, stationId);
            ResultSet rs = prst.executeQuery();

            while (rs.next()) {
                station = extractStationFromDB(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return station;
    }

    @Override
    public List<Station> getStationBetween(Station stationA, Station stationB) {
        List<Station> myListStationId = new ArrayList<>();
        if(stationB.getId() < stationA.getId()) {
            Station temp = stationA;
            stationA = stationB;
            stationB = temp;
        }
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "select * from station where id >= " + stationA.getId() + "&& id <= " + stationB.getId();

            PreparedStatement prst = dbConnection.query(query);
            ResultSet rs = prst.executeQuery();

            while (rs.next()) {
                Station station;
                station = extractStationFromDB(rs);
                myListStationId.add(station);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return myListStationId;
    }

    private Station extractStationFromDB(ResultSet rs) throws SQLException {
        Station station;
        int id = rs.getInt("id");
        String name = rs.getString("name");
        station = new Station(id, name);
        return station;
    }
}
