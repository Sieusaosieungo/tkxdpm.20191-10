package dao;

import entity.Station;
import entity.FareCertificate;

public interface DistanceDao {
    double getDistance(FareCertificate fareCertificate, Station station);
}
