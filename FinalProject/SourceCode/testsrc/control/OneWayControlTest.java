package control;

import entity.OneWayTicket;
import entity.Station;
import org.junit.Test;
import utils.FareCalculator;
import utils.FareCalculatorDistance;

import static org.junit.Assert.*;

public class OneWayControlTest {

    @Test
    public void checkin() {
        Station stationStarted;
        Station stationEnded;
        Station stationCurrent;
        OneWayTicket oneWayTicket;
        OneWayControl oneWayControl;

        // Error: Ticket cannot enter in this station
        stationStarted = new Station(1, "Saint-Lazare");
        stationEnded = new Station(5, "Gare de Lyon");
        stationCurrent = new Station(6, "Chatelet");
        oneWayTicket = new OneWayTicket("OW201910220000", "new", stationStarted, stationEnded,4.3);
        oneWayControl = new OneWayControl(oneWayTicket, stationCurrent);
        assertEquals("You can't enter this station", oneWayControl.checkin());

        // Error: The status of the ticket not "new" to enter platform
        stationCurrent = new Station(3, "Pyramides");
        oneWayTicket = new OneWayTicket("OW201910220000", "using", stationStarted, stationEnded,4.3);
        oneWayControl = new OneWayControl(oneWayTicket, stationCurrent);
        assertEquals("The ticket cannot enter", oneWayControl.checkin());

        // Error: none
        oneWayTicket = new OneWayTicket("OW201910220000", "new", stationStarted, stationEnded, 4.3);
        oneWayControl = new OneWayControl(oneWayTicket, stationCurrent);
        assertEquals(null, oneWayControl.checkin());
    }

    @Test
    public void checkout() {
        Station stationStarted;
        Station stationEnded;
        Station stationCurrent;
        OneWayTicket oneWayTicket;
        OneWayControl oneWayControl;
        FareCalculator fareCalculator = new FareCalculatorDistance(5.0);

        // Error: The status of the ticket not "using" status to leaves the platform area
        stationStarted = new Station(5, "Gare de Lyon");
        stationEnded = new Station(2, "Madeleine");
        stationCurrent = new Station(3, "Pyramides");
        oneWayTicket = new OneWayTicket("OW201910220001", "new", stationStarted, stationEnded, 3.1);
        oneWayControl = new OneWayControl(oneWayTicket, stationCurrent);
        assertEquals("Ticket haven't entrance, so it cannot go out", oneWayControl.checkout());

        // Error: One-way ticket haven't not enough balance
        stationStarted = new Station(1, "Saint-Lazare");
        stationEnded = new Station(5, "Gare de Lyon");
        stationCurrent = new Station(6, "Chatelet");
        oneWayTicket = new OneWayTicket("OW201910220000", "using", stationStarted, stationEnded,2.5);
        oneWayControl = new OneWayControl(oneWayTicket, stationCurrent);
        assertEquals("Not enough balance: Expected 3.1 euros", oneWayControl.checkout());

        // Error: none
        stationStarted = new Station(1, "Saint-Lazare");
        stationEnded = new Station(5, "Gare de Lyon");
        stationCurrent = new Station(6, "Chatelet");
        oneWayTicket = new OneWayTicket("OW201910220000", "using", stationStarted, stationEnded,4.3);
        oneWayControl = new OneWayControl(oneWayTicket, stationCurrent);
        assertEquals(null, oneWayControl.checkout());
    }
}