package boundary;

import control.AfcControl;
import entity.*;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;
import utils.DateHandler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

/**
 * The user interface class is used to communicate with the user, for example,
 * taking information from the user, displaying the corresponding information
 * to the user.
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class UserInterface {

    // The object represent for AfcControl class
    private AfcControl afcControl = new AfcControl();

    /**
     * show the list of stations of the system
     */
    public void showListStation() {
        List<Station> stations = afcControl.getListStation();
        for (int i = 0; i < stations.size(); i++) {
            System.out.println((char) (i + 97) + " - " + stations.get(i).getName());
        }
    }

    /**
     * get input by user include station currently and status in or out
     * in addition, user also enter barcode follow the list of item which
     * is displayed from the barcode of file
     */
    public void getInput() {
        System.out.println("These are stations in the line M14 of Paris: \n");
        this.showListStation();
        System.out.println("\nAvailable action: 1-enter station, 2-exit station");
        System.out.println(
                "\nYou can provide a combination of number (1 or 2) and a letter from (a to i) to enter or exit\n"
                        + "a station. For instance, the combination \"2-d\" will bring you to exit the station Chatelet");

        Scanner input = new Scanner(System.in);

        System.out.println("\nYour input: ");
        String stationAndIsCheckInOut = input.nextLine();
        String[] output = stationAndIsCheckInOut.split("-");

        int statusCheck = Integer.parseInt(output[0]);

        int stationId = output[1].charAt(0) - 96;

        System.out.println("These are existing tickets/cards: ");
        this.showInfoFromFile();
        System.out.println("\n\nPlease provide the ticket code you want to enter/exit: ");

        String barcodeInput = null;
        do {
            if (input.hasNextLine()) {
                barcodeInput = input.nextLine();
                if (!afcControl.getListBarcode().contains(barcodeInput)) {
                    System.out.println("You must enter a barcode in the list");
                    System.out.println("Please enter barcode again: ");
                }
            }
        } while (!afcControl.getListBarcode().contains(barcodeInput));

        afcControl = new AfcControl(statusCheck, stationId, barcodeInput);
    }

    /**
     * Displays information about tickets, cards that users have
     */
    private void showInfoFromFile() {
        List<FareCertificate> listItemInFile = afcControl.getInfoFromFile();

        for (int i = 0; i < listItemInFile.size(); i++) {
            System.out.print("\n- " + afcControl.getListBarcode().get(i) + ": ");
            FareCertificate item = listItemInFile.get(i);
            if (item instanceof OneWayTicket) {
                OneWayTicket oneWayTicket = (OneWayTicket) item;
                System.out.print("One-way Ticket");
                System.out.print(" between " + oneWayTicket.getStartStation().getName());
                System.out.print(" and " + oneWayTicket.getEndStation().getName());

                if (oneWayTicket.getStatus().equals("new")) {
                    System.out.print(": " + oneWayTicket.getStatus());
                }

                if(oneWayTicket.getStatus().equals("using")) {
                    System.out.print(": In station");
                }

                if (oneWayTicket.getStatus().equals("came-out")) {
                    System.out.print(": Destroy");
                }

                System.out.print(" - " + oneWayTicket.getBalance() + " euros");
            } else if (item instanceof Ticket24Hours) {
                Ticket24Hours ticket24Hours = (Ticket24Hours) item;
                System.out.print("24h Ticket:");
                DateHandler dateHandler = DateHandler.getInstance();
                Timestamp currentTime = dateHandler.getCurrentTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

                if (ticket24Hours.getTimeExpire() == null) {
                    System.out.print(" New");
                } else if (ticket24Hours.getTimeExpire().compareTo(currentTime) < 0) {
                    System.out.print(" Expired");
                } else {
                    System.out.print(" Valid until: " + dateFormat.format(ticket24Hours.getTimeExpire()));
                }
            } else if (item instanceof PrepaidCard) {
                PrepaidCard prepaidCard = (PrepaidCard) item;
                System.out.print("Prepaid card:");
                if(prepaidCard.getStatus().equals("using")) {
                    System.out.print(" In station");
                }
                System.out.print(" " + prepaidCard.getBalance() + " euros");
            }
        }
    }

    /**
     * perform checks, including checkin and checkout
     *
     * @throws InvalidIDException If the barcode code is converted into a card code or ticket code error occurs
     */
    public void check() throws InvalidIDException {
        String error = afcControl.check();
        FareCertificate fareCertificate = afcControl.getFareCertificate();
        if (fareCertificate instanceof OneWayTicket) {
            OneWayTicket oneWayTicket = (OneWayTicket) fareCertificate;
            System.out.println("Type: One-way ticket");
            System.out.println("ID: " + oneWayTicket.getId());
            System.out.println("Balance: " + oneWayTicket.getBalance() + " euros");
        } else if (fareCertificate instanceof Ticket24Hours) {
            Ticket24Hours ticket24Hours = (Ticket24Hours) fareCertificate;
            System.out.println("Type: 24-Hours ticket");
            System.out.println("ID: " + ticket24Hours.getId());
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
            if (ticket24Hours.getTimeExpire() != null) {
                System.out.println("Time expired: " + dateFormat.format(ticket24Hours.getTimeExpire()));
            }
        } else if (fareCertificate instanceof PrepaidCard) {
            PrepaidCard prepaidCard = (PrepaidCard) fareCertificate;
            System.out.println("Type: Prepaid card");
            System.out.println("ID: " + prepaidCard.getId());
            System.out.println("Balance: " + prepaidCard.getBalance() + " euros");
        }
        if (error == null) {
            Gate gate = Gate.getInstance();
            gate.open();
        } else {
            System.out.println(error);
        }
    }
}
