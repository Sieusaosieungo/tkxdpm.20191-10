package dao;

import entity.PrepaidCard;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * implement methods of the PPDao interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class PPDaoImpl extends FareCertificateDaoImpl implements PPDao {

    private PrepaidCard prepaidCard;

    @Override
    public PrepaidCard getInstanceByCode(String cardCode) {
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            PreparedStatement prst = dbConnection.query("SELECT * FROM AFC.Ticket card, AFC.PrepaidCard pp WHERE card.code = ? and card.id=pp.id; ");

            this.prepaidCard = extractCardFromDB(cardCode, prst);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return this.prepaidCard;
    }

    @Override
    public boolean updateBalance(String prepaidId, double balance) {
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "UPDATE `AFC`.`PrepaidCard` SET `balance` = ? WHERE (`id` = ?);\n";

            PreparedStatement prst = dbConnection.query(query);

            prst.setDouble(1, balance);
            prst.setString(2, prepaidId);
            prst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     *
     * @param key This is the ticket id of ticket. Ex: OW201910220000
     * @param prst represents a object of PrepareStatement
     * @throws SQLException if the queries to the database happen error
     */
    private PrepaidCard extractCardFromDB(String key, PreparedStatement prst) throws SQLException {
        PrepaidCard prepaidCard = null;
        prst.setString(1, key);
        ResultSet rs = prst.executeQuery();

        while(rs.next()) {
            String id = rs.getString("id");
            String type = rs.getString("type");
            String status = rs.getString("status");
            Timestamp timeUsed = rs.getTimestamp("timeUsed");
            String code = rs.getString("code");
            double balance = rs.getDouble("balance");

            prepaidCard = new PrepaidCard(id, type, status, timeUsed, code, balance);
        }

        return prepaidCard;
    }
}
