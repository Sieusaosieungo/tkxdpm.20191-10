## Tuần 4

### 1. Phân công công việc

- Vũ Duy Mạnh: Vẽ sơ đồ thực thể liên kết, thiết kế database
- Ngô Đức Minh: Thiết kế giao diện
- UN Lyan: Vẽ biểu đồ lớp chi tiết các chức năng liên quan đến thẻ
- Nguyễn Thành Luân: Vẽ biểu đồ lớp chi tiết các chức năng liên quan đến vé

### 2. Phân công review

- Minh review cho An
- An review cho Mạnh
- Mạnh review cho Luân
- Luân review cho Minh

## Tuần 5
### 1. Phân công công việc
- Tất cả các thành viên sửa lại toàn bộ các phần đã làm theo biểu đồ UC đã chỉnh sửa
- Vũ Duy Mạnh: Usecase checkin vé 1 chiều, checkin vé 24 giờ + thiết kế UI
- Ngô Đức Minh: Usecase checkout thẻ
- UN Lyan: Usecase checkin thẻ
- Nguyễn Thành Luân: Usecase checkout vé 1 chiều, checkout vé 24 giờ
- Cả nhóm thiết kế database

### 2. Phân công review

- Minh review cho An
- An review cho Mạnh
- Mạnh review cho Luân
- Luân review cho Minh
