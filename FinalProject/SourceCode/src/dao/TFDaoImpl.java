package dao;

import entity.Ticket24Hours;
import utils.DateHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * implement methods of the TFDao interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class TFDaoImpl extends FareCertificateDaoImpl implements TFDao {

    private Ticket24Hours ticket24Hours;

    @Override
    public Ticket24Hours getInstanceByCode(String ticketCode) {
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            PreparedStatement prst = dbConnection.query("SELECT * FROM AFC.Ticket tk, AFC.24HoursTicket tftk WHERE tk.code = ? and tk.id=tftk.id; ");

            this.ticket24Hours = extractTicketFromDB(ticketCode, prst);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return this.ticket24Hours;
    }

    public Timestamp updateTimeExpire(String ticketId) {
        DateHandler dateHandler = DateHandler.getInstance();
        Timestamp currentTime = dateHandler.getCurrentTime();
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            PreparedStatement prst = dbConnection.query("UPDATE `AFC`.`24HoursTicket` SET `timeExpire` = ? WHERE (`id` = ?);\n");

            prst.setTimestamp(1, dateHandler.plusTime(currentTime, 24));
            prst.setString(2, ticketId);

            prst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return currentTime;
    }

    /**
     *
     * @param key This is the ticket id of ticket. Ex: OW201910220000
     * @param prst represents a object of PrepareStatement
     * @throws SQLException if the queries to the database happen error
     */
    private Ticket24Hours extractTicketFromDB(String key, PreparedStatement prst) throws SQLException {
        Ticket24Hours ticket24Hours = null;
        prst.setString(1, key);
        ResultSet rs = prst.executeQuery();

        while(rs.next()) {
            String id = rs.getString("id");
            String type = rs.getString("type");
            String status = rs.getString("status");
            Timestamp timeUsed = rs.getTimestamp("timeUsed");
            String code = rs.getString("code");
            Timestamp timeExpire = rs.getTimestamp("timeExpire");

            ticket24Hours = new Ticket24Hours(id, type, status, timeUsed, code, timeExpire);
        }

        return ticket24Hours;
    }
}
