package control;

import entity.PrepaidCard;
import entity.Station;
import org.junit.Test;
import utils.FareCalculator;
import utils.FareCalculatorDistance;

import static org.junit.Assert.*;

public class PrepaidControlTest {

    @Test
    public void checkin() {
        Station stationCurrent;
        PrepaidCard prepaidCard;
        PrepaidControl prepaidControl;
        FareCalculator fareCalculator = new FareCalculatorDistance(5.0);

        // Error: The status of the card not "new" to enter platform
        stationCurrent = new Station(6, "Chatelet");
        prepaidCard = new PrepaidCard("PP201910220001", "using", 5);
        prepaidControl = new PrepaidControl(prepaidCard, stationCurrent);
        assertEquals("The card cannot enter", prepaidControl.checkin());

        // Error: not enough base fare
        prepaidCard = new PrepaidCard("PP201910220001", "new", 1.2);
        prepaidControl = new PrepaidControl(prepaidCard, stationCurrent);
        assertEquals("Balance on the card is less than the base fare", prepaidControl.checkin());

        // Error: none
        prepaidCard = new PrepaidCard("PP201910220000", "new", 5);
        prepaidControl = new PrepaidControl(prepaidCard, stationCurrent);
        assertEquals(null, prepaidControl.checkin());
    }

    @Test
    public void checkout() {
        Station stationCurrent;
        PrepaidCard prepaidCard;
        PrepaidControl prepaidControl;
        FareCalculator fareCalculator = new FareCalculatorDistance(5.0);

        // Error: The status of the card not "using" to enter platform
        stationCurrent = new Station(6, "Chatelet");
        prepaidCard = new PrepaidCard("PP201910220001", "new", 5);
        prepaidControl = new PrepaidControl(prepaidCard, stationCurrent);
        assertEquals("The card cannot came out", prepaidControl.checkout());

        // Error: not enough amount fare
        prepaidCard = new PrepaidCard("PP201910220000", "using", 1.2);
        prepaidControl = new PrepaidControl(prepaidCard, stationCurrent);
        assertEquals("Balance is less than the amount of the fare", prepaidControl.checkout());

        // Error: none
        prepaidCard = new PrepaidCard("PP201910220000", "using", 5);
        prepaidControl = new PrepaidControl(prepaidCard, stationCurrent);
        assertEquals(null, prepaidControl.checkout());
    }
}