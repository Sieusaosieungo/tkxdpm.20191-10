package entity;

import java.sql.Timestamp;

/**
 * <b>represents a 24Hours Ticket</b>
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class Ticket24Hours extends FareCertificate {
    private Timestamp timeExpire;

    public Ticket24Hours(){

    }

    public Ticket24Hours(String id, String status, Timestamp timeExpire) {
        super.id = id;
        super.status = status;
        this.timeExpire = timeExpire;
    }

    public Ticket24Hours(String id, String type, String status, Timestamp timeUsed, String code, Timestamp timeExpire) {
        super(id, type, status, timeUsed, code);
        this.timeExpire = timeExpire;
    }

    public Timestamp getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(Timestamp timeExpire) {
        this.timeExpire = timeExpire;
    }
}
