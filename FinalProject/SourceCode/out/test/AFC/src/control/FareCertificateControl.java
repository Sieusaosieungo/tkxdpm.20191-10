package control;

import entity.Station;
import utils.FareCalculator;

/**
 * TicketControl class is abstract class with checkin checkout methods of the user
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public abstract class FareCertificateControl {

    // station that user is using
    protected Station station;
    // method fare calculator for the trip
    protected FareCalculator fareCalculator;

    /**
     *
     * @param fareCalculator the method of fare calculator
     */
    public void setFareCalculator(FareCalculator fareCalculator) {
        this.fareCalculator = fareCalculator;
    }

    /**
     * Check if the ticket is valid before checkin
     *
     * @return error message if an error occurs, otherwise returns null
     */
    abstract protected String checkValidWhenCheckin();

    /**
     * Check if the ticket is valid before checkout
     *
     * @return error message if an error occurs, otherwise returns null
     */
    abstract protected String checkValidWhenCheckout();

    /**
     * Check if the current ticket status is a checkin status
     *
     * @return true if the current status of the ticket is valid, otherwise returns false
     */
    abstract protected boolean isCheckinStatus();

    /**
     * Check if the current ticket status is a checkout status
     *
     * @return true if the current status of the ticket is valid, otherwise returns false
     */
    abstract protected boolean isCheckoutStatus();

    /**
     *
     * @return error message if the checkin error has occurred, otherwise an error returns null
     */
    abstract String checkin();

    /**
     *
     * @return error message if the checkout error has occurred, otherwise an error returns null
     */
    abstract String checkout();
}
