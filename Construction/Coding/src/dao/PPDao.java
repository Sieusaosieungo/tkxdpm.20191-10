package dao;

import entity.PrepaidCard;

/**
 * PPDao is the interface used to communicate with prepaid card data
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface PPDao {
    /**
     * get ticket object when having the ticket id
     *
     * @param prepaidCode This is the prepaid card code.
     * @return A prepaid card object includes all the information of
     * the prepaid card object
     */
    PrepaidCard getInstanceByCode(String prepaidCode);

    /**
     * Update ticket status when checkin
     *
     * @param prepaidId This is the card id of prepaid card.
     * @return true if updating is succesful
     */
    boolean updateStatusAfterCheckin(String prepaidId);

    /**
     * Update ticket status when checkout
     *
     * @param prepaidId This is the ticket id of ticket.
     * @return true if updating is succesful
     */
    boolean updateStatusAfterCheckout(String prepaidId);
    boolean updateBalance(String prepaidId, double price);
}
