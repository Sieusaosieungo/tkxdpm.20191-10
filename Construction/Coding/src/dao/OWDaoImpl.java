package dao;

import entity.OneWayTicket;
import entity.Station;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * implement methods of the OWDao interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class OWDaoImpl extends FareCertificateDaoImpl implements OWDao  {
    private OneWayTicket oneWayTicket;

    @Override
    public OneWayTicket getInstanceByCode(String ticketCode) {
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            PreparedStatement prst = dbConnection.query("SELECT * FROM AFC.Ticket tk, AFC.OneWayTicket owtk WHERE tk.code = ? and tk.id = owtk.id; ");

            this.oneWayTicket = extractTicketFromDB(ticketCode, prst);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return this.oneWayTicket;
    }

    /**
     * @param key  This is the ticket id of ticket. Ex: OW201910220000
     * @param prst represents a object of PrepareStatement
     * @throws SQLException if the queries to the database happen error
     */
    private OneWayTicket extractTicketFromDB(String key, PreparedStatement prst) throws SQLException {
        OneWayTicket oneWayTicket = null;
        prst.setString(1, key);
        ResultSet rs = prst.executeQuery();

        while (rs.next()) {
            String id = rs.getString("id");
            String type = rs.getString("type");
            String status = rs.getString("status");
            Timestamp timeUsed = rs.getTimestamp("timeUsed");
            String code = rs.getString("code");
            int stationStarted = rs.getInt("stationStarted");
            int stationEnded = rs.getInt("stationEnded");
            double balance = rs.getDouble("balance");

            StationDao stationDao = new StationDaoImpl();
            Station stationStartedObj = stationDao.getStationById(stationStarted);
            Station stationEndedObj = stationDao.getStationById(stationEnded);
            oneWayTicket = new OneWayTicket(id, type, status, timeUsed, code, stationStartedObj, stationEndedObj, balance);
        }

        return oneWayTicket;
    }
}
