package dao;

import entity.Station;
import entity.FareCertificate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DistanceImpl implements DistanceDao {

    @Override
    public double getDistance(FareCertificate fareCertificate, Station station) {
        double distance = 0;
        DBConnection dbConnection = DBConnection.getConnection();
        TripDao tripDao = new TripDaoImpl();
        Station startStation = tripDao.getStartStation(fareCertificate.getId());
        if (startStation == null) {
            throw new Error("Can't not find start station");
        }

        int idStationA = startStation.getId();
        int idStationB = station.getId();

        if (idStationA > idStationB) {
            int temp = idStationA;
            idStationA = idStationB;
            idStationB = temp;
        }

        String query = "SELECT distance FROM distance where stationA = " + idStationA + "&& stationB = " + idStationB;

        PreparedStatement prst;
        try {
            prst = dbConnection.query(query);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                distance = rs.getDouble(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return distance;
    }
}
