package dao;

import entity.Station;

import java.util.List;

/**
 * StationDao is the interface used to communicate with station data
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface StationDao {
    /**
     * @return Full list of stations that the system has
     */
    List<Station> getAllStation();

    /**
     *
     * @param id the id of the station
     * @return Station object via id
     */
    Station getStationById(int id);

    /**
     *
     * @param stationA station A
     * @param stationB station B
     * @return The station list is between station A and station B
     */
    List<Station> getStationBetween(Station stationA, Station stationB);
}
