## Tuần 3
### 1. Phân công công việc 
* Vũ Duy Mạnh: Sửa usecase, sửa acitivty diagram, bổ sung thêm đặc tả bằng bảng, text. Vẽ biểu đồ tương tác và biểu đồ lớp cho check-out vé
* Ngô Đức Minh: Vẽ biểu đồ tương tác và biểu đồ lớp cho check-out thẻ
* UN Lyan: Vẽ biểu đồ tương tác và biểu đồ lớp cho quét vé, đọc thẻ và check-in thẻ
* Nguyễn Thành Luân: Vẽ biểu đồ tương tác và biểu đồ lớp cho check-in vé

### 2. Phân công review
* Minh review cho An
* An review cho Mạnh
* Mạnh review cho Luân
* Luân review cho Minh
