import boundary.UserInterface;
import hust.soict.se.customexception.InvalidIDException;

public class MainScreen {
    public static void main(String[] args) throws InvalidIDException {
        UserInterface ui = new UserInterface();
        ui.getInput();
        ui.check();
    }
}
