package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * implement methods of the TypeDao interface
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public class TypeDaoImpl implements TypeDao {
    @Override
    public String getType(String code) {
        String type = null;
        try {
            DBConnection dbConnection = DBConnection.getConnection();
            String query = "SELECT type FROM ticket WHERE code = ?";

            PreparedStatement prst = dbConnection.query(query);
            prst.setString(1, code);
            ResultSet rs = prst.executeQuery();

            while(rs.next()) {
                type = rs.getString("type");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return type;
    }
}
