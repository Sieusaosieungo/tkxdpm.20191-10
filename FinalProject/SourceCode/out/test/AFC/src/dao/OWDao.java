package dao;

import entity.OneWayTicket;

/**
 * OWDao is the interface used to communicate with one-way ticket data
 *
 * @author ManhVD
 * @version 2.0
 * @since 2019-10-29
 */
public interface OWDao {
    /**
     * get ticket object when having the ticket id
     *
     * @param ticketCode This is the one-way ticket code.
     * @return A one way ticket object includes all the information of
     * the one way ticket object
     */
    OneWayTicket getInstanceByCode(String ticketCode);

    /**
     * Update ticket status when checkin
     *
     * @param ticketId This is the ticket id of ticket.
     * @return true if updating is succesful
     */
    boolean updateStatusAfterCheckin(String ticketId);

    /**
     * Update ticket status when checkout
     *
     * @param ticketId This is the ticket id of ticket.
     * @return true if updating is succesful
     */
    boolean updateStatusAfterCheckout(String ticketId);
}
