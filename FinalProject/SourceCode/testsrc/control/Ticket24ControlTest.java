package control;

import entity.Station;
import entity.Ticket24Hours;
import org.junit.Test;
import utils.DateHandler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

public class Ticket24ControlTest {

    @Test
    public void checkin() {
        DateHandler dateHandler = DateHandler.getInstance();

        Timestamp currentTime = dateHandler.getCurrentTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        Timestamp timeExpire;
        Station stationCurrent = new Station(6, "Chatelet");
        Ticket24Hours ticket24Hours;
        Ticket24Control ticket24Control;

        // Error: Ticket is expired
        timeExpire = dateHandler.plusTime(currentTime, -2); // too 2 hours
        ticket24Hours = new Ticket24Hours("TF201910220000", "new", timeExpire);
        ticket24Control = new Ticket24Control(ticket24Hours, stationCurrent);
        assertEquals("Expired: Try to enter at " + dateFormat.format(currentTime), ticket24Control.checkin());

        // Error: The status of the ticket not "new" to enter platform
        timeExpire = dateHandler.plusTime(currentTime, 2); // 2 hours remaining
        ticket24Hours = new Ticket24Hours("TF201910220000", "using", timeExpire);
        ticket24Control = new Ticket24Control(ticket24Hours, stationCurrent);
        assertEquals("The ticket cannot enter", ticket24Control.checkin());

        // Error: none
        ticket24Hours = new Ticket24Hours("TF201910220000", "new", timeExpire);
        ticket24Control = new Ticket24Control(ticket24Hours, stationCurrent);
        assertEquals(null, ticket24Control.checkin());
    }

    @Test
    public void checkout() {
        DateHandler dateHandler = DateHandler.getInstance();

        Timestamp currentTime = dateHandler.getCurrentTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        Timestamp timeExpire;
        Station stationCurrent = new Station(6, "Chatelet");
        Ticket24Hours ticket24Hours;
        Ticket24Control ticket24Control;

        // Error: The status of the ticket not "using" to leave platform
        timeExpire = dateHandler.plusTime(currentTime, 2); // 2 hours remaining
        ticket24Hours = new Ticket24Hours("TF201910220000", "new", timeExpire);
        ticket24Control = new Ticket24Control(ticket24Hours, stationCurrent);
        assertEquals("Ticket haven't entrance, so it cannot go out", ticket24Control.checkout());

        // Error: none
        ticket24Hours = new Ticket24Hours("TF201910220000", "using", timeExpire);
        ticket24Control = new Ticket24Control(ticket24Hours, stationCurrent);
        assertEquals(null, ticket24Control.checkout());
    }
}